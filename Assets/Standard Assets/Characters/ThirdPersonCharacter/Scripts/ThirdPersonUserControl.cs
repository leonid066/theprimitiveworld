using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
		private bool inWater = false;
		public bool m_Chopping; 
		public float chopTime = 1.0f; 
		public float curChopTime;
		RaycastHit hit;
		public int demage = 3 ;

		public GameObject Player_main_my;
		public GameObject Axe;

		/// <summary>
		public Vector3 Correction;
		/// </summary>

		public bool IsChopping(){
			return m_Chopping;
		}

        private void Start()
        {

			Correction = new Vector3 (0, 1.5f, 0);
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
        }

		void OnTriggerEnter (Collider other){
			
			if (other.name == "WaterCollider") {
				inWater = true;
			}

			if (other.tag == "Rock_logs") {
				
			}

			if (other.tag == "Tree_logs") {
				other.SendMessage ("Picked_upLocal");
				Player_main_my.SendMessage ("ChengeResTree", 3);

			}

			if (other.tag == "Rock_logs") {
				other.SendMessage ("Picked_upLocal");
				Player_main_my.SendMessage ("ChengeResRock", 3);
			}
		}

	
		void OnTriggerExit (Collider other){
			if (other.name == "WaterCollider") {
				inWater = false;
			}
		}




        private void Update()
        {

			
			Vector3 fwd = transform.TransformDirection (Vector3.forward);
			Vector3 ModifPos = transform.position;
			ModifPos += Correction;

			Physics.Raycast (ModifPos, fwd, out hit, 2.5f);
			Transform objectHit = hit.transform;
			
			Debug.DrawRay (ModifPos, fwd, Color.red);
			if(hit.transform != null)
				Debug.DrawLine (ModifPos, hit.transform.position, Color.green);
			if ( objectHit != null)
				objectHit.SendMessage ("Aimed", null, SendMessageOptions.DontRequireReceiver);

		

            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }


			if (!m_Chopping && Input.GetMouseButtonDown (0)) {
				m_Chopping = true;
				Axe.SendMessage ("ChoppingChangeToTrue");
				if (objectHit != null) {
				//	objectHit.SendMessage ("LifeHit", demage, SendMessageOptions.DontRequireReceiver);
					Debug.Log ("Life hit to :" + objectHit.name);
				}

			}


			if ( m_Chopping && (curChopTime <= chopTime)) {
				
				curChopTime += Time.deltaTime;

			} else {
				curChopTime = 0.0f;
				m_Chopping = false; 
				Axe.SendMessage ("ChoppingChangeToFalse");
			
			}
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            bool crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif
			if (inWater)
				m_Move *=  0.5f;
            // pass all parameters to the character control script
			m_Character.Move(m_Move, crouch, m_Jump, m_Chopping);
            m_Jump = false;
        }
    }
}
