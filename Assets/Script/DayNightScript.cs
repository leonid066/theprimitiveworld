﻿using UnityEngine;
using System.Collections;

public class DayNightScript : MonoBehaviour {

	public float minutesInDay = 1.0f;

	float timer; 
	float percrntageOfDay;
	float turnSpeed;
	public AudioSource ForestSound;
	public Light MySun;

	// Use this for initialization
	void Start () {
		timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		checkTime ();
		UpdateLight ();

		turnSpeed = 360.0f / (minutesInDay * 60.0f) * Time.deltaTime;
		MySun.transform.RotateAround (MySun.transform.position, MySun.transform.right, turnSpeed);

		 // Debug.Log (percrntageOfDay* 100 + " %");
	}

	void UpdateLight(){
		if (isNight ()) {
			if (MySun.intensity > 0.0f) {
				MySun.intensity -= 0.05f;
				ForestSound.volume -= 0.05f;
			}
		} else {
			if (MySun.intensity < 1.0f) {
				MySun.intensity += 0.05f;
				if(ForestSound.volume <=0.5f)
				ForestSound.volume += 0.01f;
			}
		}
	}

	bool isNight(){
		bool c = false;
		if (percrntageOfDay > 0.5f) {
			c = true;
		} 
		return c; 
	}

	void checkTime(){
		timer += Time.deltaTime;
		percrntageOfDay = timer / (minutesInDay * 60.0f);
		if (timer > (minutesInDay * 60.0f)) {
			timer = 0.0f;
		}
	}

}
