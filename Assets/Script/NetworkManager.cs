﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	
	public Camera MainCamera;
	public string Verison; 
	public bool offline; 
	SpawnSpot[] spawnSpots;
	GameObject Player_camera; 
	GameObject character;
	 GameObject MyBase;
//	private bool IsSpawnig; 
//	private RoomInfo[] roomList;




	// Use this for initialization
	void Start () {


		spawnSpots  = GameObject.FindObjectsOfType<SpawnSpot>();
		Connect ();
	}
	void OnRecivedRoomListUpdate(){
//		roomList = PhotonNetwork.GetRoomList ();
	}

	void Connect() {
		PhotonNetwork.ConnectUsingSettings (Verison);
		PhotonNetwork.offlineMode = offline;
	}

	void OnGUI(){
		

			GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());

	}

	void OnJoinedLobby(){

		PhotonNetwork.JoinRandomRoom ();
	
	}
	void OnPhotonRandomJoinFailed (){

		PhotonNetwork.CreateRoom (null);
		
	}
	void OnJoinedRoom(){

		SpawnMyPlayer ();
	}




	void SpawnMyPlayer (){
		if (spawnSpots == null) {
			Debug.Log ("Can't find any Spawn Spots");
			return;
		}

		SpawnSpot mySpawnSpot = spawnSpots [Random.Range(0, spawnSpots.Length)];
		GameObject myPlayerGO = (GameObject) PhotonNetwork.Instantiate ("Player_main", mySpawnSpot.transform.position,  mySpawnSpot.transform.rotation, 0);

		MyBase = mySpawnSpot.Base;

		//MainCamera.GetComponent<AudioListener> ().enabled = false; 
		MainCamera.enabled = false; 
		Destroy (MainCamera);
	

		Player_camera = myPlayerGO.transform.FindChild("FreeLookCameraRig").gameObject;
		character = myPlayerGO.transform.FindChild("character").gameObject;

		if (Player_camera != null)
			Debug.Log ("We have an PlayerCamera");
		if (Player_camera == null)
			Debug.Log ("Could find the FreeLookCameraRig");
		if (character == null)
			Debug.Log ("Could find the character");
		
		Player_camera.SetActive (true);

		(character.GetComponent("ThirdPersonUserControl") as MonoBehaviour).enabled = true;
		(character.GetComponent("ThirdPersonCharacter") as MonoBehaviour).enabled = true;
		//(character.GetComponent("CameraRayCast") as MonoBehaviour).enabled = true;
		//character.GetComponent<AudioListener> ().enabled = true;

		myPlayerGO.SendMessage ("SetSpawnPos", mySpawnSpot.transform.position);
		myPlayerGO.GetComponent<NetworkCharacter> ().SetSpawnPos (mySpawnSpot.transform.position);
		//myPlayerGO.GetComponent<ResorsesControll> ().enabled = true;
		//myPlayerGO.GetComponent<ResorsesControll> ().SetMyBase (MyBase);
		myPlayerGO.GetComponent<NetworkCharacter> ().SetMyBase (MyBase);

	}

}
