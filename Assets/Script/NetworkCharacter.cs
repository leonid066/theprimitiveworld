using UnityEngine;
using UnityEngine.UI;

public class NetworkCharacter : Photon.MonoBehaviour
{
	private Vector3 correctPlayerPos = Vector3.zero; // We lerp towards this
	private Quaternion correctPlayerRot = Quaternion.identity; // We lerp towards this

	Animator m_Animator;
	public GameObject character; 
	public Vector3 SpawnPosition;

	public void SetSpawnPos(Vector3 SpawnSpotNew){
		Debug.Log ("Spawn pos:" +  SpawnPosition.x + " " + SpawnPosition.y + " " + SpawnPosition.z);
		SpawnPosition = SpawnSpotNew;
	}

	void Respawn(){
		Debug.Log (SpawnPosition);
		character.transform.position = SpawnPosition;
		character.SendMessage ("ReNewHealth");
	}


	/// <summary>
	public Text WoodGui, RockGui;
	public GameObject MyBase;
	public Transform CreatePlace;
	TeamBase MyTeamBaseScript;


	public void SetMyBase(GameObject MyBaseVal){
		MyBase = MyBaseVal;
		Debug.Log ("We got a base");
	}

	void ChengeResTree(int value){
		MyTeamBaseScript.TreeChangeLocal (value);
	}

	void ChengeResRock(int value){
		MyTeamBaseScript.RockChangeLocal (value);
	}

	void GuiChenge (){
		WoodGui.text = MyTeamBaseScript.Tree.ToString ();
		RockGui.text = MyTeamBaseScript.Rock.ToString ();
	}

	[PunRPC]
	void Build(int BuildNum){
		if(PhotonNetwork.isMasterClient){
			if (BuildNum == 1){
				if (MyTeamBaseScript.Tree >= 3) {
					PhotonNetwork.Instantiate ("Sharp_logs",CreatePlace.transform.position, CreatePlace.transform.rotation, 0);
					MyTeamBaseScript.TreeChangeLocal (-3);	
				}
			}

			if(BuildNum == 2){    // Platform
				if (MyTeamBaseScript.Tree >= 3) {
					PhotonNetwork.Instantiate ("Platform",CreatePlace.transform.position, CreatePlace.transform.rotation, 0);
					MyTeamBaseScript.TreeChangeLocal (-3);	
				}

			}
			if(BuildNum == 3){
				if (MyTeamBaseScript.Tree >= 6) {
					PhotonNetwork.Instantiate ("Platform2level", CreatePlace.transform.position, CreatePlace.transform.rotation, 0);
					MyTeamBaseScript.TreeChangeLocal (-6);	
				}

			}
			if(BuildNum == 4){
				if (MyTeamBaseScript.Rock >= 3) {
					PhotonNetwork.Instantiate ("Wall", CreatePlace.transform.position, CreatePlace.transform.rotation, 0);
					MyTeamBaseScript.RockChangeLocal (-3);
				}
			}

			MyTeamBaseScript.Chenged = true;

		}
	}

	/// </summary>


	void Start() {

		/////
		MyTeamBaseScript = MyBase.GetComponent<TeamBase> ();
		GuiChenge ();
		////

		m_Animator = character.GetComponent<Animator>();

		if (character == null)
			Debug.Log ("No character");
		if (m_Animator == null)
			Debug.Log ("No animator");
		correctPlayerPos =    character.transform.position;
		correctPlayerRot =  character.transform.rotation; 
	}

    // Update is called once per frame
    void Update()
    {
		if (!photonView.isMine) {
			character.transform.position = Vector3.Lerp (character.transform.position, this.correctPlayerPos, Time.deltaTime * 25);
			character.transform.rotation = Quaternion.Lerp (character.transform.rotation, this.correctPlayerRot, Time.deltaTime * 12);
		}

		if(photonView.isMine){
			if (Input.GetKeyDown (KeyCode.R))      {SendMessage ("Respawn", null, SendMessageOptions.DontRequireReceiver);}
			if (Input.GetKeyDown (KeyCode.Alpha1)) {photonView.RPC ("Build", PhotonTargets.AllBuffered, 1);}
			if (Input.GetKeyDown (KeyCode.Alpha2)) {photonView.RPC ("Build", PhotonTargets.AllBuffered, 2);}
			if (Input.GetKeyDown (KeyCode.Alpha3)) {photonView.RPC ("Build", PhotonTargets.AllBuffered, 3);}
			if (Input.GetKeyDown (KeyCode.Alpha4)) {photonView.RPC ("Build", PhotonTargets.AllBuffered, 4);}

			if (MyTeamBaseScript != null && MyTeamBaseScript.Chenged) {
				GuiChenge ();
				MyTeamBaseScript.Chenged = false;
			}
		}
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting) {
			stream.SendNext(character.transform.position);
			stream.SendNext(character.transform.rotation);
			stream.SendNext(m_Animator.GetFloat("Forward"));
			stream.SendNext(m_Animator.GetFloat("Turn"));
			stream.SendNext(m_Animator.GetFloat("Jump"));
			stream.SendNext(m_Animator.GetFloat("JumpLeg"));
			stream.SendNext(m_Animator.GetBool ("Crouch"));
			stream.SendNext(m_Animator.GetBool ("OnGround"));
			stream.SendNext(m_Animator.GetBool ("Chopping"));
        }

        else {
            // Network player, receive data
			this.correctPlayerPos = (Vector3)stream.ReceiveNext();
			this.correctPlayerRot = (Quaternion)stream.ReceiveNext();
			m_Animator.SetFloat("Forward", (float)stream.ReceiveNext ());
			m_Animator.SetFloat("Turn",    (float)stream.ReceiveNext ());
			m_Animator.SetFloat("Jump",    (float)stream.ReceiveNext ());
			m_Animator.SetFloat("JumpLeg", (float)stream.ReceiveNext ());
			m_Animator.SetBool( "Crouch",   (bool)stream.ReceiveNext ());
			m_Animator.SetBool( "OnGround", (bool)stream.ReceiveNext ());
			m_Animator.SetBool( "Chopping", (bool)stream.ReceiveNext ());

		
        }
    }
}
