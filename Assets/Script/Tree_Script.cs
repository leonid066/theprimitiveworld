using UnityEngine;
using System.Collections;

public class Tree_Script : MonoBehaviour  {

	Renderer rendMy;
	PhotonView photonView;
	Rigidbody myRigidBody;

	public AudioClip Chopp;
	public AudioClip Tree_fall;

	private AudioSource audioChopp;
	private AudioSource audioTree_fall;

	public float refTime;
	public int treelife;

	float aimedtimer;
	float lastSeconds;

	bool IsAimed ;
	bool treeDead; 


	bool DieDone;


	void Start () {
		
		audioChopp = AddAudio (Chopp, false, true, 1.0f);
		audioTree_fall = AddAudio (Tree_fall, false, true, 0.3f);
		audioTree_fall.pitch = 2;
		audioTree_fall.maxDistance = 15;
		audioChopp.pitch = 1.5f;
		audioChopp.maxDistance = 20;


		myRigidBody = transform.GetComponent<Rigidbody> ();
		photonView = PhotonView.Get (this);
		rendMy = transform.GetComponent<Renderer> ();

		float value = Random.value;
		transform.eulerAngles = new Vector3 (-90, value * 360.0f, 0);
		transform.localScale += new Vector3 (10 * value, 7 * value, 5 * value); 


	}

	public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol) { 
		
		AudioSource newAudio = gameObject.AddComponent<AudioSource>();

		newAudio.clip = clip; 
		newAudio.loop = loop;
		newAudio.playOnAwake = playAwake;
		newAudio.volume = vol;

		return newAudio; 

	}

	public void Aimed (){
		IsAimed = true;
	}



	[PunRPC]
	 void lifeMinus(int demage){
		
		audioChopp.Play ();

		treelife -= demage;
		if (treelife <= 0)
			treeDead = true;		

	}

	[PunRPC] 
	void TreeEnable(){
		transform.GetComponent<BoxCollider> ().enabled = false;
		transform.GetComponent<MeshRenderer> ().enabled = false;
		transform.GetComponent<AudioSource> ().enabled = false;
		transform.GetComponent<AudioSource> ().enabled = false;
		Destroy (GetComponent<Rigidbody>());
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.Destroy (gameObject);
		}
	}


	void treeDie(){
		if(lastSeconds == 0)
		audioTree_fall.Play ();
		


		myRigidBody.isKinematic = false; 
		lastSeconds += Time.deltaTime;

		if (lastSeconds >= 5.0f) {
			
			if(PhotonNetwork.isMasterClient)
				PhotonNetwork.InstantiateSceneObject ("Tree_logs", this.transform.position, Quaternion.identity, 0, null);
			photonView.RPC ("TreeEnable", PhotonTargets.AllBuffered);

			DieDone = true; 

			//gameObject.SetActive (false);
		}
	}


	 public void LifeHit(int lastdemage){
		photonView.RPC ("lifeMinus", PhotonTargets.All, lastdemage) ;
	}


	// Update is called once per frame
	void Update () {
		if (!DieDone) {
			
			if (IsAimed && aimedtimer <= refTime) {
				rendMy.material.SetColor ("_OutlineColor", Color.green);
				aimedtimer += Time.deltaTime; 
			} else if (aimedtimer > refTime) {
				IsAimed = false;
				aimedtimer = 0.0f;
				rendMy.material.SetColor ("_OutlineColor", Color.black);
			}

			if (treeDead)
				treeDie ();
		   
		}
	}
}
