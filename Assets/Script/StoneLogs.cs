﻿using UnityEngine;
using System.Collections;

public class TreeLogs : MonoBehaviour {
	PhotonView photonView;

	void Start (){
		photonView = PhotonView.Get(this);
	}

	[PunRPC]
	void Picked_up(){
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.Destroy (gameObject);
		}
	}

	void Picked_upLocal(){
		photonView.RPC("Picked_up", PhotonTargets.MasterClient);
	}
}

