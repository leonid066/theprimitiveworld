﻿using UnityEngine;
using System.Collections;

public class CloudScript : MonoBehaviour {


	// Use this for initialization
	void Start () {
		float value = Random.value;
		transform.eulerAngles = new Vector3 (270, value * 360.0f, 0);
		transform.localScale += new Vector3 (5 * value,5 * value, 5 * value); 
	}

}
