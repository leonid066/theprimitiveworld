﻿using UnityEngine;
using System.Collections;

public class TeamBase : MonoBehaviour {
	
	public int TeamID;
	public int Tree;
	public int Rock;
	public bool Chenged;
	PhotonView photonView;

	void Start(){
		photonView = PhotonView.Get (this);
	}

	[PunRPC]
	public void WoodChange(int value){Tree = value; Chenged = true;}

	[PunRPC]
	public void RockChange(int value){Rock = value; Chenged = true;}
		
	public void TreeChangeLocal(int value){
		Tree += value;
		photonView.RPC ("WoodChange",  PhotonTargets.AllBuffered ,Tree);
	}

	public void RockChangeLocal(int value){
		Rock += value;
		photonView.RPC ("RockChange", PhotonTargets.AllBuffered, Rock );
	}
		

}
